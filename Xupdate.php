<?php
/**
 * Xupdate.php
 *
 * Author: Sergii Gamaiunov <hello@webkadabra.com>
 * Date: 13.06.13
 * Time: 21:27
 */
class Xupdate extends CAction
{
	/**
	 * @var string $modelName the model class name
	 */
	public $modelName;
	public $scenario;

	/**
	 * @param string $id the id of the model to load
	 */
	public function run()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver', true);
		$es = new TbEditableSaver($this->modelName);

		if ($this->scenario)
			$es->scenario = $this->scenario;

		$es->update();
	}
}