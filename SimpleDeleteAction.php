<?php
/**
 * SimpleDeleteAction.php
 *
 * Author: Sergii Gamaiunov <hello@webkadabra.com>
 * Date: 14.06.13
 * Time: 7:59
 */
class SimpleDeleteAction extends CAction
{
	public $model = null;
	public $modelClassName = null;
	public $enableAjaxValidation = false;

    /**
     * event is raised before model is saved - you can override model's attributes here
     * @param $event
     */
    public function onAfterDelete($event) {
        $this->raiseEvent('onAfterDelete', $event);
    }

	public function run($id = null)
	{
		if(!Yii::app()->request->isPostRequest) {
			throw new CHttpException(404, 'Not Found [1]');
		}
		$id = $id ? $id : $_POST['id'];
		if ($this->modelClassName == null) {
			throw new CException('Please, specify model class name');
		}

		if ($this->model === null)
        {
			$model_class = $this->modelClassName;
			$model = new $model_class;

            $primaryKeyColumn = $model->primaryKey();

            if($primaryKeyColumn) {
			    $this->model = $model_class::model()->find($primaryKeyColumn.'=?',array($id));
            } else {
			    $this->model = $model_class::model()->findByPk($id);
            }

			//$this->model=User::model()->findbyPk($id);
			if ($this->model === null)
				throw new CHttpException(404, 'Not Found [2]');
		}

		$controller = $this->getController();


		$this->model->delete();
        if($this->hasEventHandler('onAfterDelete')){
            // create new event:
            $event = new CEvent($this);
            $this->onAfterDelete($event);
        } else {
		    if (!Yii::app()->request->isAjaxRequest)
			    $controller->redirect(Yii::app()->user->returnUrl);
        }
	}

}