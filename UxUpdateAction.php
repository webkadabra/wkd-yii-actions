<?php
/**
 * @author sergii gamaiunov <hello@webkadabra.com>
 * @since 23.03.14
 */
Yii::import('wkd.components.actions.UpdateAction');
class UxUpdateAction extends UpdateAction{
	protected function successResponse() {
		// Accessing through AJAX, return success content
		if ($this->isAjaxRequest)
		{
			// Output JSON encoded content
			JsonResponse::create()
				->setSuccessStatus()
				->setMessage($this->messages['success'])
				->setCloseModal()
				->setRefreshClientPage()
				->serve();

		} // Accessing without AJAX, redirect
		else {
			app()->user->setFlash('flashMessage', array(
				'type' => $this->flashTypePrefix . 'success',
				'content' => $this->messages['success']));
			if (!$this->isAjaxRequest)
				$this->getController()->redirect($this->getRedirectUrl($this->model->id));
		}
	}

	protected function validationErrorResponse() {
		// Accessing through AJAX, return success content
		if ($this->isAjaxRequest) {
			JsonResponse::create()
				->setErrorStatus()
				->setMessage($this->messages['error'])
				->serve();

			// Stop script execution
			Yii::app()->end();
		} // Accessing without AJAX, redirect
		else {
			app()->user->setFlash('flashMessage', array(
				'type' => $this->flashTypePrefix . 'error',
				'content' => $this->messages['error']));
		}
	}
} 