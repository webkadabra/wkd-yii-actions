<?php
class AutocompleteAction extends CAction
{
	public $model=null;
	public $modelClassName=null;
	
	public function run($id=null)
	{
		if($this->modelClassName == null) {
			throw new CException('Please, specify model class name');
		}
		
		if($this->model===null)
		{
			$this->loadModel($id);
			
			//$this->model=User::model()->findbyPk($id);
			if($this->model===null)
				throw new CHttpException(404, 'Not Found');
		}
		
		$controller = $this->getController();

		// Uncomment the following line if AJAX validation is needed
		if($this->enableAjaxValidation) {
			$this->performAjaxValidation($this->model);
		}
		if (isset($_POST[$this->modelClassName])) {
			$this->model->attributes = $_POST[$this->modelClassName];

			if ($this->model->save()) {
				//$controller->redirect(array('view', 'id' => $this->model->id));
				$controller->redirect(Yii::app()->user->returnUrl);
			}
		}
		$controller->render($this->view, array(
			'model'=>$this->model,
		));
	}
	
	protected function loadModel($id) {
		$model_class = $this->modelClassName;
		return $this->model = $model_class::model()->findByPk($id);
	}
}