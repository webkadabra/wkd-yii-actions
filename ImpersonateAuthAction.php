<?php
/**
 * This action works perfectly IF you do no have different sessions for users (i.e. admin panel is on different server)
 */
class ImpersonateCreateTokenAction extends CAction
{
	/**
     *
     */
    public function run($uid,$returnTo=null)
    {

        if(!$uid) {
            throw new CHttpException(404);
        }
		// Log back in?
		 if($uid === 'me') {

            $impersonateRealUserID = Yii::app()->user->getState('impersonateRealUserID');
            $impersonateHash = Yii::app()->user->getState('impersonateHash');
            $returnTo = Yii::app()->user->getState('impersonateContinueUrl');

            $impersonateRealUser = User::model()->findByPk($impersonateRealUserID);

            // Build hash for validation:
            if($returnTo) {
                $impersonateTestHash = md5($impersonateRealUser->id . $impersonateRealUser->attributes . Yii::app()->user->id . $returnTo);
            } else {
                $impersonateTestHash = md5($impersonateRealUser->id . $impersonateRealUser->attributes . Yii::app()->user->id);
            }
            if($impersonateTestHash !== $impersonateHash) {
                throw new CHttpException(Yii::t('error', 'Sorry, You are not authorized to do that.'), 403);
            } else {
                // Log back in
                $ui = Yii::app()->user->impersonate($impersonateRealUserID);
                if($ui) {
                    Yii::app()->user->login($ui, 0);
                    // And reset all states:
                    Yii::app()->user->setState('impersonateRealUserID', null);
                    Yii::app()->user->setState('impersonateHash', null);
                    Yii::app()->user->setState('impersonateContinueUrl', null);
                    Yii::app()->user->setState('impersonated', false);

                    $this->controller->redirect( $returnTo ? rawurldecode($returnTo) : Yii::app()->homeUrl);
                }
            }

            Yii::app()->end();
        }

        // Make sure we can access this feature
        if(!Yii::app()->user->checkAccess('impersonateAccounts'))
            throw new CHttpException(Yii::t('error', 'Sorry, You are not authorized to do that.'), 403);

        if($returnTo !== null) {
            Yii::app()->user->setState('impersonateContinueUrl', $returnTo);
        }

        // Simple hash, though this might be an overkill
        if($returnTo) {
            $impersonateHash = md5(Yii::app()->user->id . Yii::app()->user->model->attributes . $uid . $returnTo);
        } else {
            $impersonateHash = md5(Yii::app()->user->id . Yii::app()->user->model->attributes . $uid);
        }

        Yii::app()->user->setState('impersonateRealUserID', Yii::app()->user->id);
        Yii::app()->user->setState('impersonateHash', $impersonateHash);
        Yii::app()->user->setState('impersonated', true);

        $ui = Yii::app()->user->impersonate($uid);

        if($ui) {
            $result = Yii::app()->user->login($ui, 0);
        }

        //$this->redirect(Yii::app()->homeUrl);
        // Redirect to dealer's homepage:
        $this->controller->redirect('/dealer');
    }
}