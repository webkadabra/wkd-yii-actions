<?php
/**
 * 
 */
class ImpersonateByTokenAction extends CAction
{
	/**
     *
     */
    public function run($t,$returnTo=null)
    {
        if(!$token = AdminImpersonateToken::model()->find('token_key=?',array($t))) {
            throw new CHttpException(404);
        }
		if(!$token->validateTokenAccess()) {
			
			// @security
			Yii::log(
				'Attempt to hack IMPERSONATE!'
					."\n\t".'User ID: '.app()->user->id
					."\n\t".'IP: '.app()->request->userHostAddress
					."\n\t".'Token Key provided: '.$t
					."\n\t".'URI: '.app()->request->requestUri,
				'warning',
				'SecurityAlert.Catalog'
			);
			throw new CHttpException(403);
		} 
		
		// Log back in?
		// TODO implement
		/* if($t === 'me') {

            $impersonateRealUserID = Yii::app()->user->getState('impersonateRealUserID');
            $impersonateHash = Yii::app()->user->getState('impersonateHash');
            $returnTo = Yii::app()->user->getState('impersonateContinueUrl');

            $impersonateRealUser = User::model()->findByPk($impersonateRealUserID);

            // Build hash for validation:
            if($returnTo) {
                $impersonateTestHash = md5($impersonateRealUser->id . $impersonateRealUser->attributes . Yii::app()->user->id . $returnTo);
            } else {
                $impersonateTestHash = md5($impersonateRealUser->id . $impersonateRealUser->attributes . Yii::app()->user->id);
            }
            if($impersonateTestHash !== $impersonateHash) {
                throw new CHttpException(Yii::t('error', 'Sorry, You are not authorized to do that.'), 403);
            } else {
                // Log back in
                $ui = Yii::app()->user->impersonate($impersonateRealUserID);
                if($ui) {
                    Yii::app()->user->login($ui, 0);
                    // And reset all states:
                    Yii::app()->user->setState('impersonateRealUserID', null);
                    Yii::app()->user->setState('impersonateHash', null);
                    Yii::app()->user->setState('impersonateContinueUrl', null);
                    Yii::app()->user->setState('impersonated', false);

                    $this->controller->redirect( $returnTo ? rawurldecode($returnTo) : Yii::app()->homeUrl);
                }
            }

            Yii::app()->end();
        } */
		
      

        Yii::app()->user->setState('impersonateContinueUrl', $token->return_url);
        Yii::app()->user->setState('impersonateRealUserID', $token->owner_user_id);
        Yii::app()->user->setState('impersonateHash', $token->token_key);
        Yii::app()->user->setState('impersonated', true);

        $ui = Yii::app()->user->impersonate($token->target_account_id);

        if($ui) {
            //Yii::app()->user->logout();
            $result = Yii::app()->user->login($ui, 0);
			$token->used_on = new CDbExpression('NOW()');
			$token->used_by_user_ip = ip2long(Yii::app()->request->userHostAddress);
			$token->save(false);
			$token->saveCounters(array('use_count'=>1));
        }

        //$this->redirect(Yii::app()->homeUrl);
        // Redirect to dealer's homepage:
		if($result) {
			$this->controller->redirect('/');
		} else {
			echo 'Error';
			exit;
		}
	}
}