<?php
/**
 * Advanced confirm action - asks user for a confirmation
 * @author Sergii Gamaiunov <hello@webkadabra.com>
 */
class GenericModelManipulationAction extends CAction
{
	/**
	 * @var string the name of the AJAX view.
	 */
	public $ajaxView = '_view';

	/**
	 * @var boolean is this an AJAX request.
	 */
	protected $isAjaxRequest;

	/**
	 * @var string scripts which should be disabled on AJAX call.
	 */
	public $disableScripts = array();

	/**
	 * @var array user set messages for the action.
	 */
	public $messages = array();

	/**
	 * @var string message category used for Yii::t method.
	 */
	public $tCategory = 'app';

	/**
	 * @var string the name of the view.
	 */
	public $view = null;
	public $viewPrefix = null;

	public $model = null;
	public $modelClassName = null;
	public $enableAjaxValidation = false;
	public $forceControllerModelLoader = false;

	/**
	 * @var bool Whether to use user->returnUrl
	 */
	public $redirectToReturnUrl = true;

	/**
	 * @var mixed the redirect URL set by the user.
	 */
	public $redirectTo = null;

	/**
	 * @var mixed the redirect URL processed by the class method.
	 */
	private $_redirectTo = null;

	/**
	 * @var string flash messages prefix.
	 */
	public $flashTypePrefix = '';

	/**
	 * Please make sure to call this method
	 */
	protected function init()
	{
		// Create default messages array
		$defaultMessages = array(
			'postRequest' => Yii::t($this->tCategory,
				'Only post requests are allowed'),
		);

		// Merge with user set messages if array is provided
		if (is_array($this->messages)) {
			$this->messages = CMap::mergeArray(
				$defaultMessages, $this->messages);
		} else
			throw new CException(Yii::t($this->tCategory,
				'Action messages need to be an array'));

		// If view is not set, use action id for view
		if ($this->view === null)
			$this->view = $this->id;

		// Check if this is an AJAX request
		if ($this->isAjaxRequest = Yii::app()->request->isAjaxRequest) {
			// Create default array for scripts which should be disabled
			$defaultDisableScripts = array(
				'jquery.js',
				'jquery.min.js',
				'jquery-ui.min.js'
			);

			// Merge with user set scripts which should be disabled
			if (is_array($this->disableScripts)) {
				$this->disableScripts = CMap::mergeArray(
					$defaultDisableScripts, $this->disableScripts);
			} else
				throw new CException(Yii::t($this->tCategory,
					'Disable scripts need to be an array.'));

			// Disable scripts
			foreach ($this->disableScripts as $script)
				Yii::app()->clientScript->scriptMap[$script] = false;
		}

		if ($this->modelClassName == null) {
			throw new CException('Please, specify model class name');
		}
	}

	/**
	 * Render helper
	 */
	public function render($view, $data = array())
	{
		$controller = $this->getController();

		// Render view page using AJAX
		if ($this->isAjaxRequest) {
			// Output JSON encoded content
			echo CJSON::encode(array(
				'status' => 'render',
				'content' => $controller->renderPartial($this->viewPrefix . $view, $data, true, true),
			));

			// Stop script execution
			Yii::app()->end();
		} // Render view page without using AJAX
		else {
			$controller->render($this->viewPrefix . $view, $data);
		}
	}

	protected function onRun()
	{
		return;
	}

	/**
	 * Helper to load a model
	 */
	protected function loadModel($id)
	{
		$model_class = $this->modelClassName;
		if ($this->forceControllerModelLoader == true)
			$this->model = Yii::app()->controller->loadModel($id);
		else
			$this->model = CActiveRecord::model($model_class)->findByPk($id);

		if (!$this->model) {
			throw new CHttpException(404);
		}

		return $this->model;
	}

	/**
	 * Returns an URL for redirect.
	 * @param int $id the id of the model to redirect to.
	 * @return mixed processed redirect URL.
	 */
	protected function getRedirectUrl($id)
	{
		// Process redirect URL
		if ($this->_redirectTo === null) {
			// Use default redirect URL
			if ($this->redirectTo === null) {
				if ($this->redirectToReturnUrl === true) {
					$this->_redirectTo = Yii::app()->user->returnUrl;
				} else
					$this->_redirectTo = array($this->id, 'id' => $id);
			} // User set redirect URL is an array, check if id is needed
			else if (is_array($this->redirectTo)) {
				// ID is set
				if (isset($this->redirectTo['id']))
					// ID needed, set it to the model id
					if ($this->redirectTo['id'])
						$this->redirectTo['id'] = $id;
					// ID is not needed, remove it from redirect URL
					else
						unset($this->redirectTo['id']);

				// Set processed redirect URL
				$this->_redirectTo = $this->redirectTo;
			} // User set redirect URL is a string
			else
				$this->_redirectTo = $this->redirectTo;
		}

		// Return processed redirect URL
		return $this->_redirectTo;
	}
}