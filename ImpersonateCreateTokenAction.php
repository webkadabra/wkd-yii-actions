<?php
/**
 * 
 */
class ImpersonateCreateTokenAction extends CAction
{
	public $loginUrl = '/site/byKey/t/';
	/**
     *
     */
    public function run($uid,$returnTo=null)
    {
        // Log back in?
        if(!$uid) {
            throw new CHttpException(404);
        }

		// Make sure we can access this feature
        if(!Yii::app()->user->checkAccess('impersonateAccounts'))
            throw new CHttpException(Yii::t('error', 'Sorry, You are not authorized to do that.'), 403);
		
		$token = new AdminImpersonateToken;
        if($returnTo !== null) {
            $token->return_url = $returnTo;
        }


		$token->target_account_id = $uid;

		if($token->save(false)) {
			$this->controller->redirect($this->loginUrl.$token->token_key);
		} else {
			throw new CHttpException(403);
		}
    }
}