<?php
/**
 * @author Sergii Gamaiunov <hello@webkadabra.com>
 */
class SwfUploadUploadAction extends CAction
{

	public $folder;
	public $onSuccessUpload = null;
	public $modelClassName = 'UploadUserpicForm';
	public $modelAttribute = 'uploadedFile';
	public $modelImportAlias = 'application.models.form.UploadUserpicForm';

	public function run()
	{
		#Yii::trace(print_r($_REQUEST, true), 'uploader_debug');
		#Yii::trace(print_r($_FILES, true), 'uploader_debug');
		if (Yii::app()->user->isGuest) {
			if (isset($_POST[Yii::app()->session->sessionName])) {
				Yii::app()->session->close();
				Yii::app()->session->sessionID = $_POST[Yii::app()->session->sessionName];
				Yii::app()->session->open();
			}
			if (Yii::app()->user->isGuest)
				throw new CHttpException(403, 'Login!');
		}
		if ($this->modelImportAlias) {
			Yii::import($this->modelImportAlias);
		}

		#session_id($_POST['PHPSESSID']);
		#session_start();
		$folder = $this->folder;

		// Create target dir
		if (!file_exists($folder))
			@mkdir($folder, 0777, true);


		try {

			if (empty($folder) OR !file_exists($folder)) {
				throw new CException(Yii::t(__CLASS__, "Folder {$folder} does not exists.", array()));
			}
			if (isset($_FILES[$this->modelClassName]) === true) {
				$model = new $this->modelClassName;

				$attribute = $this->modelAttribute;
				$model->setAttributes(array(
					$attribute => CUploadedFile::getInstance($model, $attribute),
				));
				if (!$model->$attribute) {
					$model->$attribute = CUploadedFile::getInstance($model, $attribute); // Some times it helps
				}

				if (!$model->$attribute) {
					throw new CException(Yii::t(__CLASS__, "Could not create CUploadedFile instance. ", array()));
				}
				if ($model->validate() === false) {
					$errors = CHtml::errorSummary($model, false, false, array('firstError'));
					throw new CException(Yii::t(__CLASS__, "Invalid file. " . $errors, array()));
				}
				if (!$model->$attribute->saveAs($folder . '/' . $model->$attribute->getName())) {
					throw new CException(Yii::t(__CLASS__, "Upload error.", array()));
				} else {

					if ($this->onSuccessUpload) {
						return Yii::app()->controller->{$this->onSuccessUpload}($model);
					} else
						die("Upload success");
				}
			} else {
				throw new CException(Yii::t(__CLASS__, "File not sent.", array()));
			}
			throw new CException(Yii::t(__CLASS__, 'Unknown error.', array()));
		} catch (Exception $e) {
			throw new CException($e->getMessage());
			Yii::trace($e->getMessage(), 'uploader_debug');
			return false;
		}

	}
}