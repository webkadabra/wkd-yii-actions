<?php
/**
 * UpdateAction class file.
 *
 * @author sergii gamaiunov <hello@webkadabra.com>
 * @copyright sergii gamaiunov &copy;
 */

/**
 * UpdateAction represents an action that updates a model using normal
 * view or EUpdateDialog extension.
 *
 * @author webkadabra
 * @version 1.3
 */
class UpdateAction extends CAction
{
	public $enableAjaxValidation = false;
	/**
	 * @var string the name of the AJAX view.
	 */
	public $ajaxView = null;

	public $ajaxJsonReply = true;

	/**
	 * @var string a callback method in controller for additional processing.
	 */
	public $callback = null;
	/**
	 * @var string a callback method in controller to be called before render
	 */
	public $onBeforeFormRender = null;
	public $onBeforeFormRenderCallback = null;

	/**
	 * @var string a callback method in controller that is called after model is loaded.
	 */
	public $onModelLoaded = null;

	/**
	 * @var string scripts which should be disabled on AJAX call.
	 */
	public $disableScripts = array();

	/**
	 * @var string flash messages prefix.
	 */
	public $flashTypePrefix = '';

	/**
	 * @var boolean is this an AJAX request.
	 */
	protected $isAjaxRequest;

	/**
	 * @var array user set messages for the action.
	 */
	public $messages = array();

	/**
	 * @var mixed the redirect URL set by the user.
	 */
	public $redirectTo = null;

	/**
	 * @var mixed the redirect URL processed by the class method.
	 */
	private $_redirectTo = null;

	/**
	 * @var string message category used for Yii::t method.
	 */
	public $tCategory = 'app';

	/**
	 * @var string the name of the view.
	 */
	public $view = 'update';
	public $viewPrefix = null;
	public $viewData = array();
	public $enableComments = false;
	/** @var CActiveRecord  */
	public $model = null;
	public $modelClassName = null;
	/**
	 * @var string|null scenario for model
	 * @since 1.1
	 */
	public $scenario = null;
	public $inheritCurrentProject = false;
	public $forceControllerModelLoader = false;

	public $safe=false;

	public function onAfterSave($event) {
		$this->raiseEvent('onAfterSave', $event);
	}
	public function onBeforeSave($event) {
		$this->raiseEvent('onBeforeSave', $event);
	}

	/**
	 * Initialize the action.
	 */
	protected function init()
	{
		// Create default messages array
		$defaultMessages = array(
			'error' => Yii::t($this->tCategory,
				'There was an error while saving. Please try again.'),
			'postRequest' => Yii::t($this->tCategory,
				'Only post requests are allowed'),
			'success' => Yii::t($this->tCategory, 'Successfully updated'),
		);

		// Merge with user set messages if array is provided
		if (is_array($this->messages)) {
			$this->messages = CMap::mergeArray(
				$defaultMessages, $this->messages);
		} else
			throw new CException(Yii::t($this->tCategory,
				'Action messages need to be an array'));

		// If view is not set, use action id for view
		if ($this->view === null)
			$this->view = $this->id;

		// Check if this is an AJAX request
		if ($this->isAjaxRequest = Yii::app()->request->isAjaxRequest) {
			if (!$this->ajaxView) {
				$this->ajaxView = $this->view;
			}
			// Create default array for scripts which should be disabled
			$defaultDisableScripts = array(
				'jquery.js',
				'jquery.min.js',
				'jquery-ui.min.js'
			);

			// Merge with user set scripts which should be disabled
			if (is_array($this->disableScripts)) {
				$this->disableScripts = CMap::mergeArray(
					$defaultDisableScripts, $this->disableScripts);
			} else
				throw new CException(Yii::t($this->tCategory,
					'Disable scripts need to be an array.'));

			// Disable scripts
			foreach ($this->disableScripts as $script)
				Yii::app()->clientScript->scriptMap[$script] = false;

			// Allow only post requests
			if (!empty($_POST) && !Yii::app()->request->isPostRequest) {
				// Output JSON encoded content
				echo CJSON::encode(array(
					'status' => 'failure',
					'content' => $this->messages['postRequest'],
				));

				// Stop script execution
				Yii::app()->end();
			}
		}
	}

	protected function save() {
		// Save only if model validates
		if ($this->model->validate())
		{
			if($this->hasEventHandler('onBeforeSave')){
				// create new event:
				$event = new CEvent($this);
				$this->onBeforeSave($event);
			}

			// Save the model (DO NOT validate second time)
			if ($this->model->save(false))
			{
				$controller = $this->getController();

				// If callback is set run additional processing
				if ($this->callback !== null &&
					method_exists($controller, $this->callback)
				) {
					$controller->{$this->callback}($this->model);
				}

				if($this->hasEventHandler('onAfterSave')){
					// create new event:
					$event = new CEvent($this);
					// or we can add params:
//						$event->params = array(
//							'order_id' => $order_id,
//						);
					$this->onAfterSave($event);
				}

				$this->successResponse();
			} // Save was unsuccessful, set flash message
			else
				$this->validationErrorResponse();
		}else
			$this->validationErrorResponse();
	}

	protected function safeSave() {
		$transaction = $this->model->dbConnection->beginTransaction();
		try {
			$this->save();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			$this->model->addError($this->model->getPrimaryKey(), $e->getMessage());
			$this->validationErrorResponse();
		}
	}
	/**
	 * Run the action.
	 */
	public function run($id=null)
	{
		// Initialize the action
		$this->init();

		// Get the controller
		$controller = $this->getController();

		// the model
		if ($this->modelClassName == null) {
			throw new CException('Please, specify model class name');
			// GuesS?
			$this->modelClassName = ucfirst($controller->getId());
		}
		if ($this->model === null) {
			$this->loadModel($id);

			if ($this->model === null)
				throw new CHttpException(404, Yii::t($this->tCategory, 'Not Found'));
		}

		if ($this->scenario) {
			$this->model->scenario = $this->scenario;
		}

		// Callback?
		if ($this->onModelLoaded !== null && method_exists($controller, $this->onModelLoaded))
			$controller->{$this->onModelLoaded}($this->model);

		$this->viewData['model'] = $this->model;
		if ($this->enableAjaxValidation && Yii::app()->request->isPostRequest
		&& isset($_POST['ajax'])) {
			$controller->performAjaxValidation($this->model);
		}

		// Process submitted form:
		if (isset($_POST[$this->modelClassName]))
		{
			$this->model->attributes = $_POST[$this->modelClassName];

			if($this->safe==true) {
				$this->safeSave();
			} else {
				$this->save();
			}
		}

		$this->viewData['cancelUrl'] = $this->getRedirectUrl($this->model->id);

		// Callback?
		if ($this->onBeforeFormRender !== null && method_exists($controller, $this->onBeforeFormRender)) {
			$controller->{$this->onBeforeFormRender}($this->model);
		}

		// Callback?
		if ($this->onBeforeFormRenderCallback !== null) {
			Yii::app()->evaluateExpression($this->onBeforeFormRenderCallback,array('model'=>$this->model));
		}

		// Render update page using AJAX
		if ($this->isAjaxRequest) {
			// Output JSON encoded content
			if ($this->ajaxJsonReply==true)
				echo CJSON::encode(array(
					'status' => 'render',
					'content' => $controller->renderPartial($this->viewPrefix . $this->ajaxView, $this->viewData, true, true),
				));
			else
				$controller->renderPartial($this->viewPrefix . $this->ajaxView, $this->viewData, false, true);
			// Stop script execution
			Yii::app()->end();
		} // Render update page without using AJAX
		else
			$controller->render($this->viewPrefix . $this->view, $this->viewData);
	}

	/**
	 * Returns whether this is an AJAX request.
	 * @return boolean true if this is an AJAX request.
	 */
	public function getIsAjaxRequest()
	{
		return $this->isAjaxRequest;
	}

	/**
	 * Returns an URL for redirect.
	 * @param int $id the id of the model to redirect to.
	 * @return mixed processed redirect URL.
	 */
	protected function getRedirectUrl($id)
	{
		// Process redirect URL
		if ($this->_redirectTo === null) {
			// Use default redirect URL
			if ($this->redirectTo === null)
				$this->_redirectTo = array('view', 'id' => $id);
			// User set redirect URL is an array, check if id is needed
			else if (is_array($this->redirectTo)) {
				// ID is set
				if (isset($this->redirectTo['id']))
					// ID needed, set it to the model id
				if ($this->redirectTo['id'])
					$this->redirectTo['id'] = $id;
				// ID is not needed, remove it from redirect URL
				else
					unset($this->redirectTo['id']);

				// Set processed redirect URL
				$this->_redirectTo = $this->redirectTo;
			} // User set redirect URL is a string
			else
				$this->_redirectTo = $this->redirectTo;
		}

		// Return processed redirect URL
		return $this->_redirectTo;
	}

	protected function loadModel($id)
	{
		if(!$id)
			return null;
		$model_class = $this->modelClassName;
		if ($this->forceControllerModelLoader == true)
			return $this->model = Yii::app()->controller->loadModel($id);
		else
			return $this->model = $model_class::model()->findByPk($id);
	}

	protected function successResponse() {
		// Accessing through AJAX, return success content
		if ($this->isAjaxRequest) {
			// Output JSON encoded content
			echo CJSON::encode(array(
				'status' => 'success',
				'content' => $this->messages['success'],
			));

			// Stop script execution
			Yii::app()->end();
		} // Accessing without AJAX, redirect
		else {
			app()->user->setFlash('flashMessage', array(
				'type' => $this->flashTypePrefix . 'success',
				'content' => $this->messages['success']));
			if (!$this->isAjaxRequest)
				$this->getController()->redirect($this->getRedirectUrl($this->model->id));
		}
	}

	protected function validationErrorResponse() {
		// Accessing through AJAX, return success content
		if ($this->isAjaxRequest) {
			// Output JSON encoded content
			echo CJSON::encode(array(
				'status' => 'failure',
				'content' => $this->messages['error'],
			));

			// Stop script execution
			Yii::app()->end();
		} // Accessing without AJAX, redirect
		else {
			app()->user->setFlash('flashMessage', array(
				'type' => $this->flashTypePrefix . 'error',
				'content' => $this->messages['error']));
		}
	}
}
