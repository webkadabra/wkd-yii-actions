<?php
class CreateAction extends CAction
{
	public $model = null;
	public $modelClassName = null;
	public $enableAjaxValidation = false;
	public $onBeforeFormRender = null;
	public $onBeforeFormRenderCallback = null;
	public $onBeforeSaveCallback = null;
	public $onBeforeValidateCallback  = null;
	public $view = null;
	public $viewPrefix = null;
	public $scenario = null;
	public $viewData = array();

	public $printModelInfoOnAjaxSuccess = false;
	public $modelNameAttribute = 'name';

	/**
	 * @var mixed the redirect URL set by the user.
	 */
	public $redirectTo = null;

	/**
	 * @var mixed the redirect URL processed by the class method.
	 */
	private $_redirectTo = null;

	/**
	 * @var array user set messages for the action.
	 */
	public $messages = array();

	/**
	 * @var string scripts which should be disabled on AJAX call.
	 */
	public $disableScripts = array();

	/**
	 * @var string message category used for Yii::t method.
	 */
	public $tCategory = 'app';

	/**
	 * @var boolean is this an AJAX request.
	 */
	protected $isAjaxRequest;

	public function onAfterSave($event) {
		$this->raiseEvent('onAfterSave', $event);
	}

	/**
	 * event is raised before model is saved - you can override model's attributes here
	 * @param $event
	 */
	public function onBeforeSave($event) {
		$this->raiseEvent('onBeforeSave', $event);
	}

	/**
	 * Initialize the action.
	 */
	protected function init()
	{
		// Create default messages array
		$defaultMessages = array(
			'error' => Yii::t($this->tCategory,
				'There was an error while saving. Please try again.'),
			'postRequest' => Yii::t($this->tCategory,
				'Only post requests are allowed'),
			'success' => Yii::t($this->tCategory, 'Successfully created'),
		);

		// Merge with user set messages if array is provided
		if (is_array($this->messages)) {
			$this->messages = CMap::mergeArray(
				$defaultMessages, $this->messages);
		} else
			throw new CException(Yii::t($this->tCategory,
				'Action messages need to be an array'));

		// If view is not set, use action id for view
		if ($this->view === null)
			$this->view = $this->id;

		// Check if this is an AJAX request
		if ($this->isAjaxRequest = Yii::app()->request->isAjaxRequest) {
			// Create default array for scripts which should be disabled
			$defaultDisableScripts = array(
				'jquery.js',
				'jquery.min.js',
				'jquery-ui.min.js'
			);

			// Merge with user set scripts which should be disabled
			if (is_array($this->disableScripts)) {
				$this->disableScripts = CMap::mergeArray(
					$defaultDisableScripts, $this->disableScripts);
			} else
				throw new CException(Yii::t($this->tCategory,
					'Disable scripts need to be an array.'));

			// Disable scripts
			foreach ($this->disableScripts as $script)
				Yii::app()->clientScript->scriptMap[$script] = false;

			// Allow only post requests
			if (!empty($_POST) && !Yii::app()->request->isPostRequest) {
				// Output JSON encoded content
				echo CJSON::encode(array(
					'status' => 'error',
					'content' => $this->messages['postRequest'],
				));

				// Stop script execution
				Yii::app()->end();
			}
		}
	}

	public function run()
	{
		// Initialize the action
		$this->init();

		if ($this->modelClassName == null) {
			throw new CException('Please, specify model class name');
		}

		$controller = $this->getController();
		// get the Model Name
		//$model_class = ucfirst($controller->getId());

		// create the Model
		$this->model = new $this->modelClassName();

		// Uncomment the following line if AJAX validation is needed
		if ($this->scenario) {
			$this->model->scenario = $this->scenario;
		}

		// Callback?
		if ($this->onBeforeValidateCallback !== null) {
			Yii::app()->evaluateExpression($this->onBeforeValidateCallback ,array('model'=>$this->model));
		}

		// Uncomment the following line if AJAX validation is needed
		if ($this->enableAjaxValidation) {
			$this->performAjaxValidation($this->model);
		}

		// Process submitted form:
		if (isset($_POST[$this->modelClassName]))
		{
			$this->model->attributes = $_POST[$this->modelClassName];

			$valid = true;

			// Callback?
			if ($this->onBeforeSaveCallback !== null) {
				Yii::app()->evaluateExpression($this->onBeforeSaveCallback,array('model'=>$this->model));
			}

			// Save tags?
			if ($this->model->asa('tags') && isset($_POST['tags'])) {
				$this->model->setTags($_POST['tags']);
			}

			if($this->hasEventHandler('onBeforeSave')){
				// create new event:
				$event = new CEvent($this);
				$this->onBeforeSave($event);
			}

			if ($this->model->save())
			{
				if($this->hasEventHandler('onAfterSave')){
					// create new event:
					$event = new CEvent($this);
					$this->onAfterSave($event);
				}

				if (!Yii::app()->request->isAjaxRequest) {
					Yii::app()->user->setFlash('success', $this->messages['success']);
					$controller->redirect($this->getRedirectUrl($this->model->id));
				}

				// Sometimes we need to know the id of created model
				if ($this->printModelInfoOnAjaxSuccess === true) {
					$dump = array(
						'pk' => $this->model->primaryKey,
					);
					if ($this->modelNameAttribute) {
						$dump['name'] = $this->model->{$this->modelNameAttribute};
					}
					echo json_encode($dump);
					exit; // Vital!!
				} else {

					header('Content-Type: application/json; charset=UTF-8');
					echo CJSON::encode(array(
						'status' => 'success',
						'content' => $this->messages['success'],
					));
					Yii::app()->end();
				}
			}
		}
		$this->viewData['model'] = $this->model;

		// Callback?
		if ($this->onBeforeFormRender !== null && method_exists($controller, $this->onBeforeFormRender)) {
			$controller->{$this->onBeforeFormRender}($this->model);
		}

		// Callback?
		if ($this->onBeforeFormRenderCallback !== null) {
			Yii::app()->evaluateExpression($this->onBeforeFormRenderCallback,array('model'=>$this->model));
		}


		// Render update page using AJAX
		if ($this->isAjaxRequest) {
			// Output JSON encoded content
			echo CJSON::encode(array(
				'status' => 'render',
				'content' => $controller->renderPartial($this->viewPrefix . $this->view, $this->viewData, true, true),
			));

			// Stop script execution
			Yii::app()->end();
		} // Render update page without using AJAX
		else
			$controller->render($this->viewPrefix . $this->view, $this->viewData);
	}

	/**
	 * Returns an URL for redirect.
	 * @param int $id the id of the model to redirect to.
	 * @return mixed processed redirect URL.
	 */
	protected function getRedirectUrl($id)
	{
		// Process redirect URL
		if ($this->_redirectTo === null) {
			// Use default redirect URL
			if ($this->redirectTo === null)
				$this->_redirectTo = array('view', 'id' => $id);
			// User set redirect URL is an array, check if id is needed
			else if (is_array($this->redirectTo)) {
				// ID is set
				if (isset($this->redirectTo['id']))
					// ID needed, set it to the model id
				if ($this->redirectTo['id'])
					$this->redirectTo['id'] = $id;
				// ID is not needed, remove it from redirect URL
				else
					unset($this->redirectTo['id']);

				// Set processed redirect URL
				$this->_redirectTo = $this->redirectTo;
			} // User set redirect URL is a string
			else
				$this->_redirectTo = $this->redirectTo;
		}

		// Return processed redirect URL
		return $this->_redirectTo;
	}
}