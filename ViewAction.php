<?php
class ViewAction extends CAction
{
	/**
	 * @var string the name of the AJAX view.
	 */
	public $ajaxView = null;

	/**
	 * @var boolean is this an AJAX request.
	 */
	protected $isAjaxRequest;

	/**
	 * @var string scripts which should be disabled on AJAX call.
	 */
	public $disableScripts = array();

	/**
	 * @var array user set messages for the action.
	 */
	public $messages = array();

	/**
	 * @var string message category used for Yii::t method.
	 */
	public $tCategory = 'app';

	/**
	 * @var string the name of the view.
	 */
	public $view = null;

	public $model = null;
	public $modelClassName = null;
	public $enableAjaxValidation = false;

	public $forceControllerModelLoader = false;

	public $inheritCurrentProject = false;
	public $viewData = array();

	/**
	 * @var string a callback method in controller to be called before render
	 */
	public $onBeforeRender = null;

	/**
	 * Initialize the action.
	 */
	public function init()
	{
		// Create default messages array
		$defaultMessages = array(
			'postRequest' => Yii::t($this->tCategory,
				'Only post requests are allowed'),
		);

		// Merge with user set messages if array is provided
		if (is_array($this->messages)) {
			$this->messages = CMap::mergeArray(
				$defaultMessages, $this->messages);
		} else
			throw new CException(Yii::t($this->tCategory,
				'Action messages need to be an array'));

		// If view is not set, use action id for view
		if ($this->view === null)
			$this->view = $this->id;

		// Check if this is an AJAX request
		if ($this->isAjaxRequest = Yii::app()->request->isAjaxRequest)
		{
			if (!$this->ajaxView) {
				$this->ajaxView = $this->view;
			}
			// Create default array for scripts which should be disabled
			$defaultDisableScripts = array(
				'jquery.js',
				'jquery.min.js',
				'jquery-ui.min.js',
				'bootstrap.min.js',
			);

			// Merge with user set scripts which should be disabled
			if (is_array($this->disableScripts)) {
				$this->disableScripts = CMap::mergeArray(
					$defaultDisableScripts, $this->disableScripts);
			} else
				throw new CException(Yii::t($this->tCategory,
					'Disable scripts need to be an array.'));

			// Disable scripts
			foreach ($this->disableScripts as $script)
				Yii::app()->clientScript->scriptMap[$script] = false;
		}
		// Allow only post requests
		/* if( !Yii::app()->request->isPostRequest )
		{
			// Just render full contents for update
			if( isset( $_GET['ajax'] ) )
				$this->ajaxUpdate();

			// Output JSON encoded content
			echo CJSON::encode( array(
				'status' => 'failure',
				'content' => $this->messages['postRequest'],
			));

			// Stop script execution
			Yii::app()->end();
		} */
	}


	public function run($id = null)
	{
		// Initialize the action
		$this->init();

		if ($this->modelClassName == null) {
			throw new CException('Please, specify model class name');
		}

		if ($this->model === null) {
			$this->loadModel($id);

			//$this->model=User::model()->findbyPk($id);
			if ($this->model === null)
				throw new CHttpException(404, Yii::t($this->tCategory, 'Not Found'));
		}

		$controller = $this->getController();

		$this->onRun($controller);

		$this->viewData['model'] = $this->model;

		// Render view page using AJAX
		if ($this->isAjaxRequest) {
			// Output JSON encoded content
			echo CJSON::encode(array(
				'status' => 'render',
				'content' => $controller->renderPartial($this->ajaxView, $this->viewData, true, true),
			));

			// Stop script execution
			Yii::app()->end();
		} // Render view page without using AJAX
		else {
			$controller->render($this->view, $this->viewData);
		}

	}

	protected function onRun($controller)
	{
		// Callback?
		if ($this->onBeforeRender !== null) {
			if(is_object($this->onBeforeRender)) {
				$this->evaluateExpression($this->onBeforeRender,array($this->model));
			}
			else if(method_exists($controller, $this->onBeforeRender))
				$controller->{$this->onBeforeRender}($this->model);


		}
		return;
	}

	protected function loadModel($id)
	{
		$model_class = $this->modelClassName;
		if ($this->forceControllerModelLoader == true)
			return $this->model = Yii::app()->controller->loadModel($id);
		else
			return $this->model = $model_class::model()->findByPk($id);
	}
}