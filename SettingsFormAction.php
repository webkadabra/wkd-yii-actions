<?php
/**
 * SettingsFormAction.php
 *
 * @author: sergii gamaiunov <hello@webkadabra.com>
 * @since: 10/26/13
 */

class SettingsFormAction extends CAction
{
	public $enableAjaxValidation = false;
	/**
	 * @var string the name of the AJAX view.
	 */
	public $ajaxView = null;

	public $ajaxJsonReply = true;

	/**
	 * @var string a callback method in controller for additional processing.
	 */
	public $callback = null;
	/**
	 * @var string a callback method in controller to be called before render
	 */
	public $onBeforeFormRender = null;

	/**
	 * @var string a callback method in controller that is called after model is loaded.
	 */
	public $onModelLoaded = null;

	/**
	 * @var string scripts which should be disabled on AJAX call.
	 */
	public $disableScripts = array();

	/**
	 * @var string flash messages prefix.
	 */
	public $flashTypePrefix = '';

	/**
	 * @var boolean is this an AJAX request.
	 */
	protected $isAjaxRequest;

	/**
	 * @var array user set messages for the action.
	 */
	public $messages = array();

	/**
	 * @var mixed the redirect URL set by the user.
	 */
	public $redirectTo = null;

	/**
	 * @var mixed the redirect URL processed by the class method.
	 */
	private $_redirectTo = null;

	/**
	 * @var string message category used for Yii::t method.
	 */
	public $tCategory = 'app';

	/**
	 * @var string the name of the view.
	 */
	public $view = 'update';
	public $viewPrefix = null;
	public $viewData = array();
	public $enableComments = false;
	public $model = null;
	public $modelClassName = null;
	public $inheritCurrentProject = false;
	public $forceControllerModelLoader = false;

	/**
	 * Initialize the action.
	 */
	protected function init()
	{
		// Create default messages array
		$defaultMessages = array(
			'error' => Yii::t($this->tCategory,
				'There was an error while saving. Please try again.'),
			'postRequest' => Yii::t($this->tCategory,
				'Only post requests are allowed'),
			'success' => Yii::t($this->tCategory, 'Successfully updated'),
		);

		// Merge with user set messages if array is provided
		if (is_array($this->messages)) {
			$this->messages = CMap::mergeArray(
				$defaultMessages, $this->messages);
		} else
			throw new CException(Yii::t($this->tCategory,
				'Action messages need to be an array'));

		// If view is not set, use action id for view
		if ($this->view === null)
			$this->view = $this->id;

		// Check if this is an AJAX request
		if ($this->isAjaxRequest = Yii::app()->request->isAjaxRequest) {
			if (!$this->ajaxView) {
				$this->ajaxView = $this->view;
			}
			// Create default array for scripts which should be disabled
			$defaultDisableScripts = array(
				'jquery.js',
				'jquery.min.js',
				'jquery-ui.min.js'
			);

			// Merge with user set scripts which should be disabled
			if (is_array($this->disableScripts)) {
				$this->disableScripts = CMap::mergeArray(
					$defaultDisableScripts, $this->disableScripts);
			} else
				throw new CException(Yii::t($this->tCategory,
					'Disable scripts need to be an array.'));

			// Disable scripts
			foreach ($this->disableScripts as $script)
				Yii::app()->clientScript->scriptMap[$script] = false;

			// Allow only post requests
			if (!empty($_POST) && !Yii::app()->request->isPostRequest) {
				// Output JSON encoded content
				echo CJSON::encode(array(
					'status' => 'failure',
					'content' => $this->messages['postRequest'],
				));

				// Stop script execution
				Yii::app()->end();
			}
		}
	}

	/**
	 * Run the action.
	 */
	public function run()
	{
		// Initialize the action
		$this->init();

		// Get the controller
		$controller = $this->getController();

		// the model
		if ($this->modelClassName == null) {
			throw new CException('Please, specify model class name');
			// GuesS?
			$this->modelClassName = ucfirst($controller->getId());
		}
		if ($this->model === null) {
			$this->loadModel();

			if ($this->model === null)
				throw new CHttpException(404, Yii::t($this->tCategory, 'Not Found'));
		}

		// Callback?
		if ($this->onModelLoaded !== null && method_exists($controller, $this->onModelLoaded))
			$controller->{$this->onModelLoaded}($this->model);

		$this->viewData['model'] = $this->model;
		if ($this->enableAjaxValidation) {
			$controller->performAjaxValidation($this->model);
		}

		// Process submitted form
		if (isset($_POST[$this->modelClassName])) {
			// Get input data
			$this->model->attributes = $_POST[$this->modelClassName];

			// Save only if model validates
			if ($this->model->validate()) {
				// Save the model
				if ($this->model->save()) {
					// If callback is set run additional processing
					if ($this->callback !== null &&
						method_exists($controller, $this->callback)
					)
						$controller->{$this->callback}($this->model);

					// Accessing through AJAX, return success content
					if ($this->isAjaxRequest) {
						// Output JSON encoded content
						echo CJSON::encode(array(
							'status' => 'success',
							'content' => $this->messages['success'],
						));

						// Stop script execution
						Yii::app()->end();
					} // Accessing without AJAX, redirect
					else {
						app()->user->setFlash('flashMessage', array(
							'type' => $this->flashTypePrefix . 'success',
							'content' => $this->messages['success']));
						if (!$this->isAjaxRequest)
							$controller->redirect($this->getRedirectUrl());
					}
				} // Save was unsuccessful, set flash message
				else
					app()->user->setFlash('flashMessage', array(
						'type' => $this->flashTypePrefix . 'error',
						'content' => $this->messages['error']));
			}
		}

		$this->viewData['cancelUrl'] = $this->getRedirectUrl();

		// Callback?
		if ($this->onBeforeFormRender !== null && method_exists($controller, $this->onBeforeFormRender)) {
			$controller->{$this->onBeforeFormRender}($this->model);
		}

		// Render update page using AJAX
		if ($this->isAjaxRequest) {
			// Output JSON encoded content
			if ($this->ajaxJsonReply)
				echo CJSON::encode(array(
					'status' => 'render',
					'content' => $controller->renderPartial($this->viewPrefix . $this->ajaxView, $this->viewData, true, true),
				));
			else
				$controller->renderPartial($this->viewPrefix . $this->ajaxView, $this->viewData);
			// Stop script execution
			Yii::app()->end();
		} // Render update page without using AJAX
		else
			$controller->render($this->viewPrefix . $this->view, $this->viewData);
	}

	/**
	 * Returns whether this is an AJAX request.
	 * @return boolean true if this is an AJAX request.
	 */
	public function getIsAjaxRequest()
	{
		return $this->isAjaxRequest;
	}

	/**
	 * Returns an URL for redirect.
	 * @param int $id the id of the model to redirect to.
	 * @return mixed processed redirect URL.
	 */
	protected function getRedirectUrl()
	{
		// Process redirect URL
		if ($this->_redirectTo === null) {
			// Use default redirect URL
			if ($this->redirectTo === null)
				$this->_redirectTo = app()->user->returnUrl;
			// User set redirect URL is an array, check if id is needed
			else if (is_array($this->redirectTo)) {

				// Set processed redirect URL
				$this->_redirectTo = $this->redirectTo;
			} // User set redirect URL is a string
			else
				$this->_redirectTo = $this->redirectTo;
		}

		// Return processed redirect URL
		return $this->_redirectTo;
	}

	protected function loadModel()
	{
		$model_class = $this->modelClassName;
		return $this->model = new $model_class;

	}
}