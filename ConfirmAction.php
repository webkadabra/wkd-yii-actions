<?php
require __DIR__ . DIRECTORY_SEPARATOR . 'GenericModelManipulationAction.php';
/**
 * Advanced confirm action - asks user for a confirmation
 * @author Sergii Gamaiunov <hello@webkadabra.com>
 */
class ConfirmAction extends GenericModelManipulationAction
{
	/**
	 * The action will render a delete confirmation view.
	 */
	const CONFIRM_RENDER = 0;

	/**
	 * The action will delete loaded model.
	 */
	const CONFIRM_PROCEED = 1;

	/**
	 * The action will cancel the delete.
	 */
	const CONFIRM_CANCEL = 2;

	/**
	 * @var int the status for the action to proceed with.
	 */
	private $_status = self::CONFIRM_RENDER;

	/**
	 * @var string the name of the AJAX view.
	 */
	public $ajaxView = null;

	/**
	 * @var string the name of the view.
	 */
	public $view = 'confirm';

	/**
	 * @var mixed the redirect URL used after successful delete.
	 */
	public $redirectToAfterDelete = array('admin');

	/**
	 * @var null Model function that will be called when user confirms his action
	 */
	public $callModelFunc = null;
	public $refreshOnSuccess = false;

	/**
	 * @var null callback to be called before rendering page
	 */
	public $beforeRenderCallback = null;

	public $viewData = array();
	public $model = null;

	/**
	 * Initialize the action.
	 */
	protected function init()
	{
		parent::init();

		// Create default messages array
		$defaultMessages = array(
			'cancel' => Yii::t($this->tCategory, 'Action canceled'),
			'error' => Yii::t($this->tCategory,
				'There was an error while performing an action. Please try again.'),
			'postRequest' => Yii::t($this->tCategory,
				'Only post requests are allowed'),
			'success' => Yii::t($this->tCategory, 'Successfully done'),
		);

		// Merge with user set messages if array is provided
		if (is_array($this->messages)) {
			$this->messages = CMap::mergeArray(
				$defaultMessages, $this->messages);
		} else
			throw new CException(Yii::t($this->tCategory,
				'Action messages need to be an array'));


		if (isset($_POST['confirmAction']) || (isset($_POST['action']) && $_POST['action'] == 'confirmAction')) {
			$this->_status = self::CONFIRM_PROCEED;
		} else if (isset($_POST['denyAction']) || (isset($_POST['action']) && $_POST['action'] == 'denyAction')) {
			$this->_status = self::CONFIRM_CANCEL;
		}
	}

	protected function doThings()
	{

		if ($this->callModelFunc !== null) {
			return $this->model->{$this->callModelFunc}();
		}
		return true;
	}

	/**
	 * @JSON.response.status:
	 */
	public function run($id = null)
	{
		// Initialize the action
		$this->init();

		if (!$this->model) {
			$this->model = $this->loadModel($id);
		}

		// Get the controller
		$controller = $this->getController();
		// Delete is confirmed, proceed
		if ($this->_status === self::CONFIRM_PROCEED) {
			// The model deleted successfully
			if ($this->doThings()) {
				// Accessing through AJAX, return success content
				if ($this->isAjaxRequest) {
					if ($this->refreshOnSuccess) {
						$this->messages['success'] .= '
						 <script>
        				$(document).ready(function() {
           					location.reload();
						});
						</script>';
					}
					// Output JSON encoded content
					echo CJSON::encode(array(
						'status' => 'done',
						'content' => $this->messages['success'],
					));

					// Stop script execution
					Yii::app()->end();
				} // Accessing without AJAX, redirect
				else {
					Yii::app()->user->setFlash('flashMessage', array(
						'type' => $this->flashTypePrefix . 'success',
						'content' => $this->messages['success']
					));
					$controller->redirect($this->redirectToAfterDelete);
				}
			} // Deletion was unsuccessful, set flash message
			else
				Yii::app()->user->setFlash('flashMessage', array(
					'type' => $this->flashTypePrefix . 'error',
					'content' => $this->messages['error']));
		} // Delete is canceled
		else if ($this->_status === self::CONFIRM_CANCEL) {
			// Accessing through AJAX, return cancel content
			if ($this->isAjaxRequest) {
				// Output JSON encoded content
				echo CJSON::encode(array(
					'status' => 'canceled',
					'content' => $this->messages['cancel'],
				));

				// Stop script execution
				Yii::app()->end();
			} // Accessing without AJAX, redirect
			else {
				$controller->redirect($this->getRedirectUrl($this->model->id));
			}
		}

		// Callback?
		if ($this->beforeRenderCallback !== null && method_exists($controller, $this->beforeRenderCallback)) {

			$controller->{$this->beforeRenderCallback}($this->model);
		}

		$this->viewData['model'] = $this->model;

		// Render delete page using AJAX
		if ($this->isAjaxRequest) {
			// Output JSON encoded content
			echo CJSON::encode(array(
				'status' => 'render',
				'content' => $controller->renderPartial(($this->ajaxView ? $this->ajaxView : $this->view), $this->viewData, true, true),
			));

			// Stop script execution
			Yii::app()->end();
		} // Render delete page without using AJAX
		else
			$controller->render($this->view, $this->viewData);

	}

}