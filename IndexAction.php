<?php
class IndexAction extends CAction
{
	public $model=null;
	public $modelClassName=null;
	public $enableAjaxValidation=false;
	public $view='index';
	public $inheritCurrentProject=false;
	public $viewCreateModelVar='createModel';
	
	public $useViewFilters=false;
	
	public function run($id=null)
	{
		if($this->modelClassName == null) {
			throw new CException('Please, specify model class name');
		}
		$model_class = $this->modelClassName;
		
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			unset($_GET['pageSize']);
		}
		
		// Get data to pun in $model_class list
		$model=new $model_class('search');

		/* if(method_exists(Yii::app()->controller, 'defaultAttributes') && !isset($_GET[$model_class]) && !isset($_GET['clearFilters'])) {
			// Get default attributes
			if($model->asa('ERememberFiltersBehavior')) {
				if(!$model->asa('ERememberFiltersBehavior')->readSearchValues()){
					$model->attributes = CMap::mergeArray($model->attributes, Yii::app()->controller->defaultAttributes());
				}	
			} else
				$model->attributes = CMap::mergeArray($model->attributes, Yii::app()->controller->defaultAttributes());
		} */
		if($model->asa('ERememberFiltersBehavior')) {
			// Remember Grid Filters Behavior requires us not to unset attributes
		} else {
			if(isset($_GET[$model_class]))
				$model->attributes=$_GET[$model_class];
			else
				$model->unsetAttributes();
		}
		$dataProvider = $model->search();
		$this->controller->render($this->view,array(
			'model'=>$model,
			'dataProvider'=>$dataProvider,
		));
	}

}