<?php
require __DIR__ . DIRECTORY_SEPARATOR . 'GenericModelManipulationAction.php';
/**
 * Advanced delete action - asks user for a confirmation
 * @author Sergii Gamaiunov <hello@webkadabra.com>
 */
class DeleteAction extends GenericModelManipulationAction
{
	/**
	 * The action will render a delete confirmation view.
	 */
	const DELETE_RENDER = 0;

	/**
	 * The action will delete loaded model.
	 */
	const DELETE_PROCEED = 1;

	/**
	 * The action will cancel the delete.
	 */
	const DELETE_CANCEL = 2;

	/**
	 * @var int the status for the action to proceed with.
	 */
	private $_status = self::DELETE_RENDER;

	/**
	 * @var string the name of the AJAX view.
	 */
	public $ajaxView = null;

	/**
	 * @var mixed the redirect URL used after successful delete.
	 */
	public $redirectToAfterDelete = array('admin');

	/**
	 * Initialize the action.
	 */
	protected function init()
	{
		parent::init();

		if ($this->ajaxView == null) {
			$this->ajaxView = $this->view;
		}
		// Create default messages array
		$defaultMessages = array(
			'cancel' => Yii::t($this->tCategory, 'Deletion canceled'),
			'error' => Yii::t($this->tCategory,
				'There was an error while deleting. Please try again.'),
			'postRequest' => Yii::t($this->tCategory,
				'Only post requests are allowed'),
			'success' => Yii::t($this->tCategory, 'Successfully deleted'),
		);

		// Merge with user set messages if array is provided
		if (is_array($this->messages)) {
			$this->messages = CMap::mergeArray(
				$defaultMessages, $this->messages);
		} else
			throw new CException(Yii::t($this->tCategory,
				'Action messages need to be an array'));


		if (isset($_POST['confirmDelete']) || (isset($_POST['action']) && $_POST['action'] == 'confirmDelete')) {
			$this->_status = self::DELETE_PROCEED;
		} else if (isset($_POST['denyDelete']) || (isset($_POST['action']) && $_POST['action'] == 'denyDelete')) {
			$this->_status = self::DELETE_CANCEL;
		}
	}

	/**
	 * @JSON.response.status:
	 */
	public function run($id = null)
	{
		// Initialize the action
		$this->init();

		$model = $this->loadModel($id);

		// Get the controller
		$controller = $this->getController();
		// Delete is confirmed, proceed
		if ($this->_status === self::DELETE_PROCEED) {
			// The model deleted successfully
			if ($model->delete()) {
				// Accessing through AJAX, return success content
				if ($this->isAjaxRequest) {
					// Output JSON encoded content
					echo CJSON::encode(array(
						'status' => 'deleted',
						'content' => $this->messages['success'],
					));

					// Stop script execution
					Yii::app()->end();
				} // Accessing without AJAX, redirect
				else {
					Yii::app()->user->setFlash('flashMessage', array(
						'type' => $this->flashTypePrefix . 'success',
						'content' => $this->messages['success']
					));
					$controller->redirect($this->redirectToAfterDelete);
				}
			} // Deletion was unsuccessful, set flash message
			else
				$controller->setFlash('flashMessage', array(
					'type' => $this->flashTypePrefix . 'error',
					'content' => $this->messages['error']));
		} // Delete is canceled
		else if ($this->_status === self::DELETE_CANCEL) {
			// Accessing through AJAX, return cancel content
			if ($this->isAjaxRequest) {
				// Output JSON encoded content
				echo CJSON::encode(array(
					'status' => 'canceled',
					'content' => $this->messages['cancel'],
				));

				// Stop script execution
				Yii::app()->end();
			} // Accessing without AJAX, redirect
			else {
				$controller->redirect($this->getRedirectUrl($model->id));
			}
		}

		// Render delete page using AJAX
		if ($this->isAjaxRequest) {
			// Output JSON encoded content
			echo CJSON::encode(array(
				'status' => 'render',
				'content' => $controller->renderPartial($this->ajaxView, array(
					'model' => $model), true, true),
			));

			// Stop script execution
			Yii::app()->end();
		} // Render delete page without using AJAX
		else
			$controller->render($this->view, array(
				'model' => $model
			));

	}

}